#include "machine.c"
#include <gtest/gtest.h>

TEST(State0, Machine0){
    ASSERT_EQ(goNORTE, MaquinaDeEstados(NO_CARROS,goNORTE));
}
TEST(State1, Machine1){
    ASSERT_EQ(goNORTE, MaquinaDeEstados(CARROS_NORTE,goNORTE));
}
TEST(State2, Machine2){
    ASSERT_EQ(waitNORTE, MaquinaDeEstados(CARROS_ESTE,goNORTE));
}
TEST(State3, Machine3){
    ASSERT_EQ(waitNORTE, MaquinaDeEstados(AMBOS_LADOS,goNORTE));
}



TEST(State4, Machine4){
    ASSERT_EQ(goESTE, MaquinaDeEstados(NO_CARROS,waitNORTE));
}
TEST(State5, Machine5){
    ASSERT_EQ(goESTE, MaquinaDeEstados(CARROS_ESTE,waitNORTE));
}
TEST(State6, Machine6){
    ASSERT_EQ(goESTE, MaquinaDeEstados(CARROS_NORTE,waitNORTE));
}
TEST(State7, Machine7){
    ASSERT_EQ(goESTE, MaquinaDeEstados(AMBOS_LADOS,waitNORTE));
}



TEST(State8, Machine8){
        ASSERT_EQ(goESTE, MaquinaDeEstados(NO_CARROS,goESTE));
}
TEST(State9, Machine9){
        ASSERT_EQ(goESTE, MaquinaDeEstados(CARROS_ESTE,goESTE));    
}
TEST(State10, Machine10){
        ASSERT_EQ(waitESTE, MaquinaDeEstados(CARROS_NORTE,goESTE));
}
TEST(State11, Machine11){
        ASSERT_EQ(waitESTE, MaquinaDeEstados(AMBOS_LADOS,goESTE));
}



TEST(State12, Machine12){
        ASSERT_EQ(goNORTE, MaquinaDeEstados(NO_CARROS,waitESTE));
}
TEST(State13, Machine13){
        ASSERT_EQ(goNORTE, MaquinaDeEstados(CARROS_ESTE,waitESTE));
}
TEST(State14, Machine14){
        ASSERT_EQ(goNORTE, MaquinaDeEstados(CARROS_NORTE,waitESTE));
}
TEST(State15, Machine15){
        ASSERT_EQ(goNORTE, MaquinaDeEstados(AMBOS_LADOS,waitESTE));
}


int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
